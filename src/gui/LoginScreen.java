package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import request.LoginRequest;
import request.RegisterRequest;

import client.ClientConnectorInterface;
import client.ClientServiceObject;

public class LoginScreen extends JPanel implements ActionListener {
	protected JButton register , login , exit;
	protected JTextArea msg;
	protected JTextField user;
	protected JPasswordField pass;

	/* Service related fields */
	private ClientConnectorInterface connector;
	private ClientServiceObject cso;

	public LoginScreen(ClientConnectorInterface connector) {
		this.connector = connector;
		cso = ClientServiceObject.getInstance(connector);

		Icon middleButtonIcon = new SquareIcon(Color.BLUE);
		//this.setBorder(new LineBorder(Color.red, 2));

		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		register = new JButton("REGISTER", middleButtonIcon);
		register.setVerticalTextPosition(AbstractButton.BOTTOM);
		register.setHorizontalTextPosition(AbstractButton.CENTER);
		register.setToolTipText("Click this button to register.");
		register.setPreferredSize( new Dimension(100 , 50));

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;

		c.insets = new Insets(0,0,0,0);
		add(register, c);


		middleButtonIcon = new SquareIcon(Color.GREEN);
		login = new JButton("LOGIN", middleButtonIcon);
		login.setVerticalTextPosition(AbstractButton.BOTTOM);
		login.setHorizontalTextPosition(AbstractButton.CENTER);
		login.setToolTipText("Click this button to login.");
		login.setPreferredSize( new Dimension(100 , 50));

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 2;
		c.gridy = 0;
		c.insets = new Insets(0, 0, 0, 0);
		add(login, c);


		JTextArea tArea = new JTextArea("User");
		tArea.setEditable(false);
		Font font = new Font("Verdana", Font.BOLD, 12);
		tArea.setFont(font);    
		c.gridx = 1;
		c.gridy = 1;
		c.insets = new Insets(0,50,0,0);
		add(tArea, c);

		user = new JTextField(12);
		c.gridx = 1;
		c.gridy = 2;
		c.insets = new Insets(0, 0, 0, 0);
		add(user, c);


		tArea = new JTextArea("Password");
		tArea.setEditable(false);
		font = new Font("Verdana", Font.BOLD, 12);
		tArea.setFont(font);    
		c.gridx = 1;
		c.gridy = 3;
		c.insets = new Insets(0,35,0,0);
		add(tArea, c);

		pass = new JPasswordField(12);
		c.gridx = 1;
		c.gridy = 4;
		c.insets = new Insets(0, 0, 0, 0);
		add(pass, c);


		middleButtonIcon = new SquareIcon(Color.RED);
		exit = new JButton("EXIT", middleButtonIcon);
		exit.setVerticalTextPosition(AbstractButton.BOTTOM);
		exit.setHorizontalTextPosition(AbstractButton.CENTER);
		exit.setToolTipText("Click this button to EXIT.");

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 5;
		c.insets = new Insets(20, 0, 0, 0);
		add(exit, c);


		msg = new JTextArea("");
		msg.setEditable(false);
		font = new Font("Verdana", Font.BOLD, 12);
		msg.setFont(font);    
		c.gridx = 1;
		c.gridy = 6;
		c.insets = new Insets(20,0,0,0);
		add(msg, c);


		//Listen for actions on buttons 1 and 3.
		register.addActionListener(this);
		login.addActionListener(this);
		exit.addActionListener(this);


	}

	public void actionPerformed(ActionEvent e) {

		if("LOGIN".equals(e.getActionCommand())){
			login();
		}
		if("REGISTER".equals(e.getActionCommand())){
			register();
		}
		if("EXIT".equals(e.getActionCommand())){
			System.exit(ABORT);
		}

	}

	public int login(){
		if(user.getText().length() == 0 || pass.getText().length() == 0){
			msg.setText("LOGIN failed.");
			return 1;
		}
		String response = cso.checkUser(user.getText(), new String(pass.getPassword()));
		System.out.println("Response from login = " + response);
		if (response.equals(LoginRequest.LOGIN_OK)) {
			userLogged(user.getText(), new String(pass.getPassword()));
			return 0;
		} else {
			msg.setText("LOGIN failed.");
			return 1;
		}
	}

	public int register(){
		System.out.println("Apel functia de REGISTER");

		if(user.getText().length() == 0 || pass.getText().length() == 0){
			msg.setText("REGISTER failed.");
			return 1;
		}

		String response = cso.registerUser(user.getText(), new String(pass.getPassword()));
		System.out.println("Response from register = " + response);
		if (response.equals(RegisterRequest.REGISTER_OK)) {
			userLogged(user.getText(), new String(pass.getPassword()));
			return 0;
		} else {
			msg.setText("REGISTER failed.");
			return 1;
		}
	}
	
	public void userLogged(String username, String password){
		this.setVisible(false);
		this.removeAll();
		
		UserScreen newUserScreen = new UserScreen(connector, username, password);
		newUserScreen.setOpaque(true); //content panes must be opaque
						
		this.removeAll();
		this.add(newUserScreen);
		this.setVisible(true);
	}

	class SquareIcon implements Icon {
		private static final int SIZE = 14;
		Color culoare = null;

		public SquareIcon(Color _color){
			super();
			this.culoare = _color;
		}

		public void paintIcon(Component c, Graphics g, int x, int y) {
			if (c.isEnabled()) {
				g.setColor(this.culoare);
			} else {
				g.setColor(Color.GRAY);
			}

			g.fillRect(x, y, SIZE, SIZE);
		}

		public int getIconWidth() {
			return SIZE;
		}

		public int getIconHeight() {
			return SIZE;
		}
	}
}