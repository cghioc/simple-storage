package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileFilter;

import request.LockRequest;

import client.ClientConnectorInterface;
import client.ClientServiceObject;

import common.StorageProps;

public class UserScreen extends JPanel implements ActionListener {
	protected JButton edit , save , close , download , upload , share , exit;
	protected JTextArea msg = null;

	protected JList listFiles = null;
	protected DefaultListModel model;
	private boolean editareFisier = false;

	private boolean UDP = false;
	private boolean TCP = true;
	protected JButton udp , tcp;

	protected JTextArea fisier = null;

	protected JFileChooser chooser;
	protected String choosertitle;
	private ClientConnectorInterface connector;
	private ClientServiceObject cso;
	private String username, password;

	public UserScreen(ClientConnectorInterface connector, String username, String password) {
		this.connector = connector;
		this.username = username;
		this.password = password;
		this.cso = ClientServiceObject.getInstance(connector);

		if (StorageProps.SOCKET_TYPE.equals(StorageProps.SOCKET_TCP)) {
			System.out.println("Comunicatie prin tcp");
			TCP = true;
			UDP = false;
		} else {
			System.out.println("Comunicatie prin udp");
			TCP = false;
			UDP = true;
		}

		Icon middleButtonIcon = new SquareIcon(Color.BLUE);

		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		edit = new JButton("EDIT", middleButtonIcon);
		edit.setVerticalTextPosition(AbstractButton.BOTTOM);
		edit.setHorizontalTextPosition(AbstractButton.CENTER);
		edit.setToolTipText("Click this button to edit the selected file.");
		edit.setPreferredSize( new Dimension(110 , 50));

		c.gridx = 0;
		c.gridy = 0;

		c.insets = new Insets(0,0,0,0);
		add(edit, c);
		edit.addActionListener(this);


		save = new JButton("SAVE");
		save.setVerticalTextPosition(AbstractButton.BOTTOM);
		save.setHorizontalTextPosition(AbstractButton.CENTER);
		save.setToolTipText("Click this button to save the selected file.");
		save.setPreferredSize( new Dimension(110 , 20));
		save.setEnabled( false );

		c.gridx = 0;
		c.gridy = 1;

		c.insets = new Insets(5,0,0,0);
		add(save, c);
		save.addActionListener(this);


		close = new JButton("CLOSE");
		close.setVerticalTextPosition(AbstractButton.BOTTOM);
		close.setHorizontalTextPosition(AbstractButton.CENTER);
		close.setToolTipText("Click this button to close the selected file.");
		close.setPreferredSize( new Dimension(110 , 20));
		close.setEnabled( false );

		c.gridx = 0;
		c.gridy = 2;

		c.insets = new Insets(5,0,0,0);
		add(close, c);
		close.addActionListener(this);


		download = new JButton("DOWNLOAD", middleButtonIcon);
		download.setVerticalTextPosition(AbstractButton.BOTTOM);
		download.setHorizontalTextPosition(AbstractButton.CENTER);
		download.setToolTipText("Click this button to download the selected file.");
		download.setPreferredSize( new Dimension(110 , 50));

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 3;

		c.insets = new Insets(10,0,0,0);
		add(download, c);
		download.addActionListener(this);


		upload = new JButton("UPLOAD", middleButtonIcon);
		upload.setVerticalTextPosition(AbstractButton.BOTTOM);
		upload.setHorizontalTextPosition(AbstractButton.CENTER);
		upload.setToolTipText("Click this button to upload a file.");
		upload.setPreferredSize( new Dimension(110 , 50));

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 4;

		c.insets = new Insets(10,0,0,0);
		add(upload, c);
		upload.addActionListener(this);


		share = new JButton("SHARE", middleButtonIcon);
		share.setVerticalTextPosition(AbstractButton.BOTTOM);
		share.setHorizontalTextPosition(AbstractButton.CENTER);
		share.setToolTipText("Click this button to share the selected file.");
		share.setPreferredSize( new Dimension(110 , 50));

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 5;

		c.insets = new Insets(10,0,0,0);
		add(share, c);
		share.addActionListener(this);

		middleButtonIcon = new SquareIcon(Color.RED);
		exit = new JButton("EXIT", middleButtonIcon);
		exit.setVerticalTextPosition(AbstractButton.BOTTOM);
		exit.setHorizontalTextPosition(AbstractButton.CENTER);
		exit.setToolTipText("Click this button to EXIT.");
		exit.setPreferredSize( new Dimension(110 , 50));

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 6;

		c.insets = new Insets(10,0,0,0);
		add(exit, c);
		exit.addActionListener(this);


		//--------------------------------------------------------------//
		//--------------------------------------------------------------//


		listFiles = new JList();
		model = new DefaultListModel();
		listFiles = new JList(model);

		JScrollPane pane = new JScrollPane(listFiles);
		Dimension size = new Dimension(200, 500);
		pane.setPreferredSize(size);
		pane.setMinimumSize(size);

		/* Call server to get all user's files */
		ArrayList<String> userFiles = cso.listFiles(username, password);
		if (userFiles != null)
			for (String fileName : userFiles)
				model.addElement(fileName);

		c.gridx = 1;
		c.gridy = 0;
		c.gridheight = 8;
		c.fill = GridBagConstraints.VERTICAL;

		c.insets = new Insets(0,10,0,0);
		add(pane, c);


		//--------------------------------------------------------------//
		//--------------------------------------------------------------//

		fisier = new JTextArea("TEXT");
		fisier.setEditable(false);
		Font font = new Font("Verdana", Font.BOLD, 12);
		fisier.setFont(font);
		fisier.setBorder( BorderFactory.createMatteBorder(2,2,2,2,Color.GRAY) );

		JScrollPane paneText = new JScrollPane(fisier);
		Dimension sizeText = new Dimension(600, 500);
		paneText.setPreferredSize(sizeText);
		paneText.setMinimumSize(sizeText);

		c.gridx = 2;
		c.gridy = 0;
		c.gridheight = 8;
		c.fill = GridBagConstraints.VERTICAL;
		c.insets = new Insets(0,20,0,0);
		add(paneText, c);


		//--------------------------------------------------------------//
		//--------------------------------------------------------------//

		middleButtonIcon = new SquareIcon(Color.GRAY);
		if( UDP )
			middleButtonIcon = new SquareIcon(Color.ORANGE);
		udp = new JButton("UDP" , middleButtonIcon);
		udp.setVerticalTextPosition(AbstractButton.BOTTOM);
		udp.setHorizontalTextPosition(AbstractButton.LEFT);
		udp.setToolTipText("Click this button to select UDP transfer mode.");
		udp.setPreferredSize( new Dimension(80 , 20));

		c.gridx = 0;
		c.gridy = 8;

		c.insets = new Insets(5,0,0,0);
		add(udp, c);
		udp.addActionListener(this);

		middleButtonIcon = new SquareIcon(Color.GRAY);
		if( TCP ) 
			middleButtonIcon = new SquareIcon(Color.ORANGE);
		tcp = new JButton("TCP" , middleButtonIcon);
		tcp.setVerticalTextPosition(AbstractButton.BOTTOM);
		tcp.setHorizontalTextPosition(AbstractButton.LEFT);
		tcp.setToolTipText("Click this button to select TCP transfer mode.");
		tcp.setPreferredSize( new Dimension(80 , 20));

		c.gridx = 1;
		c.gridy = 8;

		c.insets = new Insets(5,0,0,0);
		add(tcp, c);
		tcp.addActionListener(this);

	}

	public void actionPerformed(ActionEvent e) {
		System.out.println(e.getActionCommand() +" command on : "+listFiles.getSelectedValue());

		boolean cevaSelectat = true;
		if(listFiles.getSelectedValue()== null)
			cevaSelectat = false;

		if("EXIT".equals(e.getActionCommand())){
			System.exit(ABORT);
		}
		String fileName = (String)listFiles.getSelectedValue();
		System.out.println("S-a selectat fisierul " + fileName);

		if("UDP".equals(e.getActionCommand())){
			UDP = true;
			TCP = false;
			tcp.setIcon( new SquareIcon(Color.GRAY));
			udp.setIcon( new SquareIcon(Color.ORANGE));
		}

		if("TCP".equals(e.getActionCommand())){
			UDP = false;
			TCP = true;
			tcp.setIcon( new SquareIcon(Color.ORANGE));
			udp.setIcon( new SquareIcon(Color.GRAY));
		}

		if("EDIT".equals(e.getActionCommand()) && cevaSelectat){
			if (cevaSelectat == false)
				return;

			/* Download the file */
			File file = cso.downloadFile(username, fileName);
			if (file == null) {
				System.out.println("File null");
				return;
			}

			System.out.println("Download ok");

			/* Lock the file */
			System.out.println("Locking file " + fileName + " for user " + username);
			String bool = cso.lockFile(username, fileName);
			if (bool.equals(LockRequest.LOCK_FAIL)) {
				System.out.println("File couldn't be locked");
				return;
			}

			System.out.println("Lock ok");

			String string_fisier = null;
			DataInputStream in = null;

			byte[] buffer = new byte[(int) file.length()];
			try {
				in = new DataInputStream(new FileInputStream(file));

				in.readFully(buffer);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			string_fisier = new String(buffer);

			fisier.setText( string_fisier );


			System.out.println("Edit ok");

			editareFisier = true;
			fisier.setEditable( true );
			save.setEnabled( true );
			close.setEnabled( true );
			listFiles.setEnabled( false );
		}
		if("DOWNLOAD".equals(e.getActionCommand()) && cevaSelectat && !editareFisier){
			download.setEnabled( false);

			if (cevaSelectat == false){
				download.setEnabled( true );
				return;
			}

			listFiles.setEnabled( false );
			System.out.println("Downloading file " + fileName);
			File file = cso.downloadFile(username, fileName);
			listFiles.setEnabled( true );

			if (file == null) {
				System.out.println("Downloaded file is null");
				download.setEnabled( true );
				return;
			}

			File folder = new File(username);
			if(!folder.exists())
				folder.mkdir();

			File out_file = new File(username+"/"+fileName);
			if(out_file.exists())
				out_file.delete();

			System.out.println(out_file.getAbsolutePath());

			try {
				FileInputStream fin = new FileInputStream(file);
				FileOutputStream fout = new FileOutputStream(out_file , true);

				System.out.println("\n\t\t am creat fout");

				int ch;
				while( (ch = fin.read()) != -1)
					fout.write(ch);

				fin.close();
				fout.close();

			} catch (IOException ex) {}

			System.out.println("\tDOWNLOAD - OKEY");
			download.setEnabled( true );
		}

		if("SAVE".equals(e.getActionCommand()) && cevaSelectat){
			save.setEnabled( false );

			/* Save the file */
			try {
		        BufferedWriter out = new BufferedWriter(new FileWriter("temporal.txt"));
		        out.write(fisier.getText());
		        out.close();
		    } 
		    catch (IOException e1) 
		    { 
		       System.out.println("Exception ");
		    }
			
			File file = new File("temporal.txt");			
			
			System.out.println("Save file " + fileName + " for user " + username);
			Boolean bool = cso.saveFile(username, fileName , file);
			if (!bool) {
				System.out.println("File couldn't be saved");
				return;
			}
			file.delete();

			System.out.println("Unlock ok");

			save.setEnabled( true );
			System.out.println("\tSAVE - OKEY");
		}

		if("CLOSE".equals(e.getActionCommand()) && cevaSelectat){
			editareFisier = false;

			
			fisier.getText();
			
			/* Save the file */
			try {
		        BufferedWriter out = new BufferedWriter(new FileWriter("temporal.txt"));
		        out.write(fisier.getText());
		        out.close();
		    } 
		    catch (IOException e1) 
		    { 
		       System.out.println("Exception ");
		    }
			
			File file = new File("temporal.txt");			
			
			System.out.println("Save file " + fileName + " for user " + username);
			Boolean bool = cso.saveFile(username, fileName , file);
			if (!bool) {
				System.out.println("File couldn't be saved");
				return;
			}
			file.delete();

			System.out.println("Unlock ok");
			
			/* Unlock the file */
			System.out.println("Unlocking file " + fileName + " for user " + username);
			Boolean bool1 = cso.unlockFile(username, fileName);
			if (!bool) {
				System.out.println("File couldn't be unlocked");
				return;
			}

			System.out.println("Unlock ok");
			
			fisier.setText( new String("NO FILE LOADED") );

			listFiles.setEnabled( true );
			fisier.setEditable( false );
			save.setEnabled( false );
			close.setEnabled( false );
			System.out.println("\tCLOSE - OKEY");
		}

		if("UPLOAD".equals(e.getActionCommand()) && !editareFisier){
			listFiles.setEnabled( false );
			upload.setEnabled( false );

			choosertitle = new String("Choose file to upload");

			chooser = new JFileChooser(); 
			chooser.setCurrentDirectory(new java.io.File("."));
			chooser.setDialogTitle(choosertitle);
			//chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			//
			// disable the "All files" option.
			//
			chooser.setFileFilter(new FileFilter() {

				@Override
				public String getDescription() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean accept(File f) {
					if( f.isDirectory() )
						return true;
					if (f.getName().toLowerCase().endsWith("txt"))
					{
						return true;
					}				    
					return false;
				}
			});
			//    
			if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) { 
				System.out.println("getCurrentDirectory(): "
						+  chooser.getCurrentDirectory());
				System.out.println("getSelectedFile() : "
						+  chooser.getSelectedFile());
			}
			else {
				System.out.println("No Selection ");
			}	
			Boolean uploaded = cso.uploadFile(username,
					chooser.getSelectedFile().getName(), chooser.getSelectedFile());
			System.out.println("S-a uploadat? " + uploaded);
			listFiles.setEnabled( true );
			upload.setEnabled( true );
			System.out.println("\tUPLOAD - OKEY");			
		}

		if("SHARE".equals(e.getActionCommand()) && cevaSelectat && !editareFisier){
			listFiles.setEnabled( false );

			JOptionPane popUp = new JOptionPane();


			Object result = popUp.showInputDialog(null, "Insert friend's ID:", "Share", JOptionPane.PLAIN_MESSAGE);

			if(result != null){

				System.out.println("Friend name: "+ result);
			}

			listFiles.setEnabled( true );
			System.out.println("\tSHARE - OKEY");
		}

	}
	

	class SquareIcon implements Icon {
		private static final int SIZE = 14;
		Color culoare = null;

		public SquareIcon(Color _color){
			super();
			this.culoare = _color;
		}

		public void paintIcon(Component c, Graphics g, int x, int y) {
			if (c.isEnabled()) {
				g.setColor(this.culoare);
			} else {
				g.setColor(Color.GRAY);
			}

			g.fillOval(x, y, SIZE, SIZE);
		}

		public int getIconWidth() {
			return SIZE;
		}

		public int getIconHeight() {
			return SIZE;
		}
	}
}