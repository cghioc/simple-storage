package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import client.ClientConnectorInterface;
import client.ClientServiceObject;
import client.TCPClientConnector;
import client.NIOTCPClientConnector;

import common.ServerDetails;
import common.StorageProps;

public class ServerSelectScreen extends JPanel implements ActionListener {
	private static final long serialVersionUID = 8616217294063361282L;

	protected JButton exit;

	private boolean[] stareServere = new boolean[StorageProps.SERVER_NO];

	private JFrame mainFrame = null;

	public ServerSelectScreen() {

		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		Icon middleButtonIcon = new SquareIcon(Color.GREEN);

		/* Check which servers are online */
		ArrayList<ServerDetails> workingServers = ClientServiceObject.checkServers();

		for (int nrServer = 0; nrServer < StorageProps.SERVER_NO; nrServer++){
			this.stareServere[nrServer] = false;
			System.out.println("Stare server " + workingServers.get(nrServer).getState());
			if (workingServers.get(nrServer).getState())
				this.stareServere[nrServer] = true;
		}

		for (int nrServer = 1; nrServer <= StorageProps.SERVER_NO; nrServer++){

			JButton serverX = new JButton("SERVER "+nrServer, middleButtonIcon);

			if( stareServere[nrServer-1] == false )
				serverX.setEnabled(false);

			serverX.setVerticalTextPosition(AbstractButton.BOTTOM);
			serverX.setHorizontalTextPosition(AbstractButton.CENTER);
			serverX.setToolTipText("Click this button to select SERVER "+nrServer+".");

			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = nrServer-1;
			c.insets = new Insets(10,0,0,0);
			add(serverX, c);
			serverX.addActionListener(this);

		}

		middleButtonIcon = new SquareIcon(Color.RED);
		exit = new JButton("EXIT", middleButtonIcon);
		exit.setVerticalTextPosition(AbstractButton.BOTTOM);
		exit.setHorizontalTextPosition(AbstractButton.CENTER);
		exit.setToolTipText("Click this button to EXIT.");

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = StorageProps.SERVER_NO + 1;
		c.insets = new Insets(20, 0, 0, 0);
		add(exit, c);
		exit.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {

		if("EXIT".equals(e.getActionCommand())){
			System.exit(ABORT);
		}
		else{
			int serverID = Integer.parseInt( e.getActionCommand().subSequence(7 , e.getActionCommand().length()).toString() ) ;			
			System.out.println("Server "+serverID+" selectat.");

			if(stareServere[serverID-1] == true){
				this.setVisible(false);

				ServerDetails server = StorageProps.getServers().get(serverID - 1);
				Thread connector;
				if (StorageProps.TCP_SOCKET_TYPE.equals(StorageProps.TCP_SOCKET_NIO))
					connector = new NIOTCPClientConnector(server.getIP(), server.getPort());
				else
					connector = new TCPClientConnector(server.getIP(), server.getPort());
				connector.start();
				try {
					Thread.sleep(200);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				LoginScreen newLoginScreen = new LoginScreen((ClientConnectorInterface)connector);
				newLoginScreen.setOpaque(true); //content panes must be opaque
								
				this.removeAll();
				this.add(newLoginScreen);
				this.setVisible(true);
			}
		}

	}


	public void createAndShowGUI() {
		JFrame.setDefaultLookAndFeelDecorated(true);

		mainFrame = new JFrame("Proiect PDSD");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setOpaque(true);
		
		//mainFrame.add(newContentPane);
		mainFrame.add(this);

		mainFrame.pack();
		mainFrame.setSize(new Dimension(1100, 600));
		mainFrame.setVisible(true);
	}

	class SquareIcon implements Icon {
		private static final int SIZE = 14;
		Color culoare = null;

		public SquareIcon(Color _color){
			super();
			this.culoare = _color;
		}

		public void paintIcon(Component c, Graphics g, int x, int y) {
			if (c.isEnabled()) {
				g.setColor(this.culoare);
			} else {
				g.setColor(Color.GRAY);
			}

			g.fillRect(x, y, SIZE, SIZE);
		}

		public int getIconWidth() {
			return SIZE;
		}

		public int getIconHeight() {
			return SIZE;
		}
	}
}