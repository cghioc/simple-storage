package gui;

import common.StorageProps;

public class ClientGUI {
	private static ServerSelectScreen serverScreen = null;
	
	public static void main(String[] args) {
		StorageProps.loadProperties();

		serverScreen = new ServerSelectScreen();
		serverScreen.createAndShowGUI();
	}
}
