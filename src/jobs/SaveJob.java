package jobs;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;

import client.CallbackInterface;

public class SaveJob implements Runnable {
	private String username, filename;
	private CallbackInterface ci;
	private int ID_SERVER;
	private int req_type;
	private File file;

	public SaveJob(String username, String filename, File file, CallbackInterface ci , int _ID_SERVER , int _req_type) {
		this.username = username;
		this.filename = filename;
		this.file = file;
		this.ci = ci;
		this.ID_SERVER = _ID_SERVER;
		this.req_type = _req_type;
	}

	public void run() {

		File folder = new File("server"+this.ID_SERVER+"_data/"+this.username);
		File out_file = new File("server"+this.ID_SERVER+"_data/"+this.username+"/"+filename);

		System.out.println("\n\t\t server"+this.ID_SERVER+"_data/"+this.username+"/"+filename);

		synchronized(this){
			if( !folder.exists())
				folder.mkdir();

			if( out_file.exists() ){
				
				System.out.println("\n\t\t Fac save pe: server"+this.ID_SERVER+"_data/"+this.username+"/"+filename);

				try {
					String string_fisier = null;
					DataInputStream in = null;

					byte[] buffer = new byte[(int) file.length()];
					try {
						in = new DataInputStream(new FileInputStream(file));

						in.readFully(buffer);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					string_fisier = new String(buffer);
					
					FileWriter fw = new FileWriter("server"+this.ID_SERVER+"_data/"+this.username+"/"+filename);
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(string_fisier);
					bw.close();

				} catch (IOException e) {
					this.ci.sendResult(false);
				}
			}
		}

		this.ci.sendResult(true);
	}
}