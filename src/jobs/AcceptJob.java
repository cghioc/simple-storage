package jobs;

import java.nio.channels.*;

import server.MultithreadedNIOTCPServer;


public class AcceptJob implements Runnable {
	private MultithreadedNIOTCPServer server;
	private SelectionKey key;
	
	public AcceptJob(MultithreadedNIOTCPServer server, SelectionKey key) {
		this.server = server;
		this.key = key;
	}
	
	public void run() {
		try {
			this.server.doAccept(this.key);
		} catch (Exception e) {
			System.err.println("AcceptJob exception: " + e);
			e.printStackTrace();
		}
	}
}
