package jobs;

import java.util.concurrent.Callable;


import java.io.File;
import java.io.IOException;

import client.CallbackInterface;

public class UnlockJob implements Runnable {
	private String username, filename;
	private CallbackInterface ci;
	private int ID_SERVER;
	private int req_type;
	private File file;

	public UnlockJob(String username, String filename, CallbackInterface ci , int _ID_SERVER , int _req_type) {
		this.username = username;
		this.filename = filename;
		this.file = file;
		this.ci = ci;
		this.ID_SERVER = _ID_SERVER;
		this.req_type = _req_type;
	}

	public void run() {

		File lock_file = new File("server"+this.ID_SERVER+"_data/"+this.username+"/"+filename+".lock");

		synchronized(this){

			if( !lock_file.exists() )
				this.ci.sendResult( new Boolean (false) );
			else
				lock_file.delete();

		}
		this.ci.sendResult( new Boolean (true) );
	}
}