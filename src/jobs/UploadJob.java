package jobs;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;

import client.CallbackInterface;

public class UploadJob implements Runnable {
	private String username, filename;
	private CallbackInterface ci;
	private int ID_SERVER;
	private int req_type;
	private File file;

	public UploadJob(String username, String filename, File file, CallbackInterface ci , int _ID_SERVER , int _req_type) {
		this.username = username;
		this.filename = filename;
		this.file = file;
		this.ci = ci;
		this.ID_SERVER = _ID_SERVER;
		this.req_type = _req_type;
	}

	public void run() {

		File folder = new File("server"+this.ID_SERVER+"_data/"+this.username);
		File out_file = new File("server"+this.ID_SERVER+"_data/"+this.username+"/"+filename);
		File out_file_locked = new File("server"+this.ID_SERVER+"_data/"+this.username+"/"+filename+".lock");

		System.out.println("\n\t\t server"+this.ID_SERVER+"_data/"+this.username+"/"+filename);

		synchronized(this){
			if( !folder.exists())
				folder.mkdir();

			System.out.println("\n\t\tserver"+this.ID_SERVER+"_data/"+this.username+"/"+filename+".lock");
			if(out_file_locked.exists()){
				System.out.println("\n\t\tserver"+this.ID_SERVER+"_data/"+this.username+"/"+this.filename+" are LOCK");
				this.ci.sendResult(false);
				return;
			}

			if( out_file.exists() )
				out_file.delete();

			try {
				FileInputStream fin = new FileInputStream(file);
				FileOutputStream fout = new FileOutputStream(out_file , true);

				System.out.println("\n\t\t am creat fout");

				int ch;
				while( (ch = fin.read()) != -1)
					fout.write(ch);

				fin.close();
				fout.close();

			} catch (IOException e) {
				this.ci.sendResult(false);
			}
		}

		this.ci.sendResult(true);
	}
}