package jobs;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;

import org.omg.CORBA.Request;

import common.ServerDetails;
import common.StorageProps;

import request.LoginRequest;
import server.ServerMulticastBroadcaster;
import server.ServerResponseConnector;
import ch.ethz.ssh2.transport.ClientServerHello;
import client.CallbackInterface;
import client.ClientConnectorInterface;
import client.ClientServiceObject;
import client.NIOTCPClientConnector;

public class LoginJob implements Runnable{
	private String username, password;
	private CallbackInterface ci;
	private int source_port = -1;
	private int ID_SERVER = -1;
	private int req_type;

	public LoginJob(String username, String password, CallbackInterface ci, int _source_port , int _ID_SERVER , int _req_type) {
		this.username = username;
		this.password = password;
		this.ci = ci;
		this.source_port = _source_port;
		this.ID_SERVER = _ID_SERVER;
		this.req_type = _req_type;
	}

	public void run() {

		boolean user_pe_server_curent = false;
		boolean user_pe_server_vecin = false;

		try{
			FileInputStream fstream = new FileInputStream("server"+this.ID_SERVER+"_data/users_s"+this.ID_SERVER+".info");

			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			while ((strLine = br.readLine()) != null)					
				if( strLine.split(" ")[0].equals( this.username)  &&  strLine.split(" ")[1].equals( this.password) ){
					user_pe_server_curent = true;
					break;
				}

			in.close();

		}catch (Exception e){
			System.err.println("Error: " + e.getMessage());
		}

		String response = LoginRequest.LOGIN_FAIL;
		
		
		
		System.out.print("\n\t[ServiceObject  "+this.ID_SERVER+"] Req type: "+this.req_type+"  ");
		if(this.req_type == request.Request.CLIENT_SERVER)
			System.out.println("CLIENT_SERVER");
		else
			System.out.println("SERVER_SERVER");
		
		
		
		if(!user_pe_server_curent && this.req_type == request.Request.CLIENT_SERVER){
						
			ServerMulticastBroadcaster smb = new ServerMulticastBroadcaster(this.ID_SERVER, 
						StorageProps.MULTICAST_ADDR, StorageProps.MULTICAST_PORT);
			
			LoginRequest logReq = new LoginRequest(username, password);
			logReq.setType(request.Request.SERVER_SERVER);
			ServerDetails thisServer = StorageProps.getServers().get(this.ID_SERVER);
			logReq.setIP(thisServer.getIP());
			logReq.setPort(thisServer.getPort()+100*this.ID_SERVER);
			
			ServerResponseConnector src = new ServerResponseConnector(thisServer.getPort()+100*this.ID_SERVER);
			src.start();
			
			smb.sendServerMulticast(logReq);
			
			ArrayList<ServerDetails> workingServers = ClientServiceObject.checkServers();
			int nrServerUp = 0;
			
			for(ServerDetails server:workingServers)
				if(server.getState())
					nrServerUp++;
			
			System.out.println("\n\t[ServiceObject  "+this.ID_SERVER+"] "+nrServerUp);
			for(int i=0 ; i< nrServerUp ; i++){
				String answer = (String) src.getAnswer();
				System.err.println("\tServer "+i+" mi-a raspuns "+answer);
				if( answer.equals(LoginRequest.LOGIN_OK) )
					user_pe_server_vecin = true;
			}
			
			src.stop();
		}


		if(user_pe_server_vecin || user_pe_server_curent)
			response = LoginRequest.LOGIN_OK;

		System.out.println("\n\t[ServiceObject  "+this.ID_SERVER+"] User: "+this.username+"  "+response);

		this.ci.sendResult(response);
	}
}