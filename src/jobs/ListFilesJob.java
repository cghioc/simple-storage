package jobs;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

import client.CallbackInterface;

public class ListFilesJob implements Runnable {
	private String username, password;
	private CallbackInterface ci;
	private int ID_SERVER;
	private int req_type;

	public ListFilesJob(String username, String password, CallbackInterface ci , int _ID_SERVER , int _req_type) {
		this.username = username;
		this.password = password;
		this.ci = ci;
		this.ID_SERVER = _ID_SERVER;
		this.req_type = _req_type;
	}

	public void run() {
		ArrayList<String> userFiles = new ArrayList<String>();

		File dir = new File("server"+this.ID_SERVER+"_data/"+this.username);

		if( dir.exists() ){

			String[] files = dir.list(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String filename) {
					return filename.endsWith(".txt");
				}
			});

			for(int i=0 ; i<files.length ; i++ )
				userFiles.add( files[i] );
		}

		this.ci.sendResult(userFiles);
	}
}
