package jobs;

import java.io.File;
import java.io.IOException;

import request.LockRequest;

import client.CallbackInterface;

public class LockJob implements Runnable {
	private String username, filename;
	private CallbackInterface ci;
	private int ID_SERVER;
	private int req_type;
	private File file;

	public LockJob(String username, String filename, CallbackInterface ci , int _ID_SERVER , int _req_type) {
		this.username = username;
		this.filename = filename;
		this.file = file;
		this.ci = ci;
		this.ID_SERVER = _ID_SERVER;
		this.req_type = _req_type;
	}

	public void run() {

		String TRUE = LockRequest.LOCK_OK;;
		String FALSE = LockRequest.LOCK_FAIL;

		File out_file = new File("server"+this.ID_SERVER+"_data/"+this.username+"/"+filename+".lock");

		synchronized(this){

			if( !out_file.exists() ){
				try {
					out_file.createNewFile();					
				} catch (IOException e) {
					System.out.println("\n\t\t Am iesit cu Exceptie");
					e.printStackTrace();
				}
				System.out.println("\n\t\tAm iesit cu TRUE");
				this.ci.sendResult( TRUE );					
				return;
			}else{
				System.out.println("\n\t\tAm iesit cu FALSE");
				this.ci.sendResult( FALSE );
			}
		}
		System.out.println("\n\t\t sync done!");
	}
}