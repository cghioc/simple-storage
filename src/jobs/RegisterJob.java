package jobs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import request.RegisterRequest;
import client.CallbackInterface;

public class RegisterJob implements Runnable{
	private String username, password;
	private CallbackInterface ci;
	private int ID_SERVER;
	private int req_type;

	public RegisterJob(String username, String password, CallbackInterface ci, int _ID_SERVER , int _req_type) {
		this.username = username;
		this.password = password;
		this.ci = ci;
		this.ID_SERVER = _ID_SERVER;
		this.req_type = _req_type;
	}

	public void run() {

		boolean user_pe_server_curent = false;
		boolean user_pe_server_vecin = false;

		try{
			FileInputStream fstream = new FileInputStream("server"+this.ID_SERVER+"_data/users_s"+this.ID_SERVER+".info");

			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			while ((strLine = br.readLine()) != null)					
				if( strLine.split(" ")[0].equals( this.username)  &&  strLine.split(" ")[1].equals( this.password) ){
					user_pe_server_curent = true;
					break;
				}

			in.close();

		}catch (Exception e){
			System.err.println("Error: " + e.getMessage());
		}

		String response = RegisterRequest.REGISTER_FAIL;
		
		System.out.println("\n\t[ServiceObject  "+this.ID_SERVER+"] User: "+this.username+"  "+user_pe_server_curent+" "+user_pe_server_vecin);

		// TODO
		// user_pe_server_vecin
		
		if(this.req_type == request.Request.CLIENT_SERVER){
		
		}
		
		if( !user_pe_server_curent && !user_pe_server_vecin){
			response = RegisterRequest.REGISTER_OK;
			
			// REGISTER USER
			try {
			    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("server"+this.ID_SERVER+"_data/users_s"+this.ID_SERVER+".info", true)));
			    out.println(this.username+" "+this.password);
			    out.close();
			} catch (IOException e) {}
			
			// CRETEA USER FOLDER ON CURRENT SERVER
			File dir = new File("server"+this.ID_SERVER+"_data/"+this.username);
			if(!dir.exists() && !dir.mkdir() )
				System.out.println("\n\t[ServiceObject  "+this.ID_SERVER+"] ERROR CREATING FOLDER");
		}

		System.out.println("\n\t[ServiceObject] User: "+this.username+"  "+response);
		this.ci.sendResult(response);
	}
}
