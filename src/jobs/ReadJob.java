package jobs;

import java.nio.channels.*;

import server.MultithreadedNIOTCPServer;


public class ReadJob implements Runnable {
	private MultithreadedNIOTCPServer server;
	private SelectionKey key;
	
	public ReadJob(MultithreadedNIOTCPServer server, SelectionKey key) {
		this.server = server;
		this.key = key;
	}
	
	public void run() {
		try {
			this.server.doRead(this.key);
		} catch (Exception e) {
			System.err.println("ReadJob exception: " + e);
			e.printStackTrace();
		}
	}
}
