package jobs;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.ArrayList;

import client.CallbackInterface;

public class DownloadJob implements Runnable {
	private String username, filename;
	private CallbackInterface ci;
	private int ID_SERVER;
	private int req_type;

	public DownloadJob(String username, String filename, CallbackInterface ci , int _ID_SERVER , int _req_type) {
		this.username = username;
		this.filename = filename;
		this.ci = ci;
		this.ID_SERVER = _ID_SERVER;
		this.req_type = _req_type;
	}

	public void run() {
		
		File desiredFile = new File("server"+this.ID_SERVER+"_data/"+this.username+"/"+this.filename);

		if( desiredFile.exists() ){
			File desiredFile_locked = new File("server"+this.ID_SERVER+"_data/"+this.username+"/"+this.filename+".lock");
			if(desiredFile_locked.exists()){
				System.out.println("\n\t\tserver"+this.ID_SERVER+"_data/"+this.username+"/"+this.filename+" are LOCK");
				this.ci.sendResult(null);
				return;
			}
			System.out.println("\n\t\tserver"+this.ID_SERVER+"_data/"+this.username+"/"+this.filename+" se trimite");
			this.ci.sendResult(desiredFile);
		}else
		if(req_type == request.Request.CLIENT_SERVER){
			// TODO
			// Caut pe servere vecine
		}

		this.ci.sendResult(null);
	}
}