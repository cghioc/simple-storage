package jobs;

import java.nio.channels.*;

import server.MultithreadedNIOTCPServer;


public class WriteJob implements Runnable {
	private MultithreadedNIOTCPServer server;
	private SelectionKey key;
	
	public WriteJob(MultithreadedNIOTCPServer server, SelectionKey key) {
		this.server = server;
		this.key = key;
	}
	
	public void run() {
		try {
			this.server.doWrite(this.key);
		} catch (Exception e) {
			System.err.println("WriteJob exception: " + e);
			e.printStackTrace();
		}
	}
}
