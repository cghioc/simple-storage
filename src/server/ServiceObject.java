package server;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import common.StorageProps;

import jobs.DownloadJob;
import jobs.LockJob;
import jobs.LoginJob;
import jobs.ListFilesJob;
import jobs.RegisterJob;
import jobs.SaveJob;
import jobs.UnlockJob;
import jobs.UploadJob;
import client.CallbackInterface;
import client.NIOCallbackObject;

public class ServiceObject implements ServiceInterface {
	private ExecutorService threadPool;
	private int source_port;
	private int ID_SERVER;
	
	public ServiceObject(int _source_port, int _ID_SERVER) {
		this.threadPool = Executors.newFixedThreadPool(StorageProps.THREAD_NO );
		this.source_port = _source_port;
		this.ID_SERVER = _ID_SERVER;
	}

	public void start() {
	}
	
	public void stop() {
		this.threadPool.shutdown();
	}

	public void checkUser(String username, String password, CallbackInterface ci , int req_type) {
		System.out.println("\n\t[ServiceObject "+this.ID_SERVER+"] S-a primit o cerere de login");
		this.threadPool.submit(new LoginJob(username, password, ci , this.source_port , this.ID_SERVER , req_type));
	}

	public void registerUser(String username, String password,
			CallbackInterface ci , int req_type) {
		System.out.println("\n\t[ServiceObject "+this.ID_SERVER+"] S-a primit o cerere de register");
		this.threadPool.submit(new RegisterJob(username, password, ci , this.ID_SERVER , req_type));
	}

	public void listFiles(String username, String password,
			CallbackInterface ci , int req_type) {
		System.out.println("\n\t[ServiceObject "+this.ID_SERVER+"] S-a primit cerere de listare fisiere");
		this.threadPool.submit(new ListFilesJob(username, password, ci , this.ID_SERVER , req_type));
	}

	public void downloadFile(String username, String filename,
			CallbackInterface ci, int req_type) {
		System.out.println("\n\t[ServiceObject "+this.ID_SERVER+"] S-a primit cerere de download fisier");
		this.threadPool.submit(new DownloadJob(username, filename, ci , this.ID_SERVER , req_type));
	}
	
	public void uploadFile(String username, String fileName, File file,
			CallbackInterface ci, int req_type) {
		System.out.println("\n\t[ServiceObject "+this.ID_SERVER+"] S-a primit cerere de upload fisier");
		this.threadPool.submit(new UploadJob(username, fileName, file, ci , this.ID_SERVER , req_type));
	}

	public void lockFile(String username, String fileName,
			CallbackInterface ci, int req_type) {
		System.out.println("\n\t[ServiceObject "+this.ID_SERVER+"] S-a primit cerere de lock pe fisier");
		this.threadPool.submit(new LockJob(username, fileName, ci, this.ID_SERVER , req_type));		
	}

	public void unlockFile(String username, String fileName,
			CallbackInterface ci, int req_type) {
		System.out.println("\n\t[ServiceObject "+this.ID_SERVER+"] S-a primit cerere de unlock pe fisier");
		this.threadPool.submit(new UnlockJob(username, fileName, ci, this.ID_SERVER , req_type));
	}

	public void saveFile(String username, String fileName, File file,
			CallbackInterface ci, int req_type) {
		System.out.println("\n\t[ServiceObject "+this.ID_SERVER+"] S-a primit cerere de save fisier");
		this.threadPool.submit(new SaveJob(username, fileName, file, ci , this.ID_SERVER , req_type));
	}
	
}
