package server;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;

import request.ChangeRequest;
import request.DownloadRequest;
import request.ListFilesRequest;
import request.LockRequest;
import request.LoginRequest;
import request.RegisterRequest;
import request.Request;
import request.SaveRequest;
import request.UnlockRequest;
import request.UploadRequest;
import client.NIOCallbackObject;
import client.TCPCallbackObject;

import common.NIOTCPSocketConnector;
import common.ServerDetails;
import common.StorageProps;
import common.Utils;

public class SimpleNIOTCPServer extends Thread implements NIOTCPSocketConnector {
	private int ID_SERVER = -1;

	private int ports;
	private boolean running;
	private Selector selector;
	private ByteBuffer rBuffer = ByteBuffer.allocate(8192);
	private ByteBuffer wBuffer = ByteBuffer.allocate(8192);

	private Hashtable<SelectionKey, ArrayList<byte[]>> writeBuffers;
	private Hashtable<SelectionKey, byte[]> readBuffers;
	private LinkedList<ChangeRequest> changeRequestQueue;

	private ArrayList<ServerSocketChannel> serverChannels;
	private ArrayList<SocketChannel> socketChannels;

	private ServiceObject sobj;

	public SimpleNIOTCPServer(int ports, ServiceObject sobj , int ID) {
		this.ID_SERVER = ID;
		this.ports = ports;
		this.sobj = sobj;

		try {
			this.selector = SelectorProvider.provider().openSelector();
		} catch (Exception e) {
			System.err.println("exceptie la crearea Selector-ului: " + e);
			e.printStackTrace();
		}

		this.rBuffer = ByteBuffer.allocate(8192);
		this.wBuffer = ByteBuffer.allocate(8192);

		this.writeBuffers = new Hashtable<SelectionKey, ArrayList<byte[]>>();
		this.readBuffers = new Hashtable<SelectionKey, byte[]>();
		this.changeRequestQueue = new LinkedList<ChangeRequest>();

		this.serverChannels = new ArrayList<ServerSocketChannel>();
		this.socketChannels = new ArrayList<SocketChannel>();
	}

	public synchronized void startRunning() {
		this.running = true;
		this.start();
	}

	public synchronized void stopRunning() {
		this.running = false;
		this.selector.wakeup();
	}

	protected synchronized boolean isRunning() {
		return this.running;
	}

	public void run() {
		System.out.println("[NIOTCPServer  "+this.ID_SERVER+"] Am pornit");

		// Creeaza director pentru useri
		File dir = new File("server"+this.ID_SERVER+"_data");
		if( !dir.exists() ) {
			dir.mkdir();

			try {
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("server"+this.ID_SERVER+"_data/users_s"+this.ID_SERVER+".info", true)));
				out.close();
			} catch (IOException e) {}
		}

		// Creeaza ServerSocketChannels.		

		try {
			ServerSocketChannel serverChannel = ServerSocketChannel.open();
			serverChannel.configureBlocking(false);
			InetSocketAddress isa = new InetSocketAddress("127.0.0.1", this.ports);
			serverChannel.socket().bind(isa);
			serverChannel.register(this.selector, SelectionKey.OP_ACCEPT);
			this.serverChannels.add(serverChannel);
		} catch (Exception e) {
			System.err.println("Exceptie la crearea ServerSocketChannel-ului: " + e);
			e.printStackTrace();
		}


		while (this.isRunning()) {
			try {
				this.selector.select(10000);

				// Proceseaza cheile pentru care s-au produs evenimente.
				Iterator<SelectionKey> selectedKeys = this.selector.selectedKeys().iterator();
				while (selectedKeys.hasNext()) {
					SelectionKey key = selectedKeys.next();
					selectedKeys.remove();

					if (!key.isValid()) {
						continue;
					}

					if (key.isAcceptable()) {
						System.out.println("[NIOTCPServer "+this.ID_SERVER+"] Eveniment de accept la cheia " + key);
						this.doAccept(key);
					}

					if (key.isWritable()) {
						System.out.println("[NIOTCPServer "+this.ID_SERVER+"] Eveniment de write_posibil la cheia " + key);
						this.doWrite(key);
					}

					if (key.isReadable()) {
						System.out.println("[NIOTCPServer "+this.ID_SERVER+"] Eveniment de read la cheia " + key);
						this.doRead(key);
					} 
				}

				// Proceseaza cererile de schimbare a operatiilor de interes.
				ChangeRequest creq;				
				synchronized (this.changeRequestQueue) {
					while ((creq = this.changeRequestQueue.poll()) != null) {
						System.out.println("[NIOTCPServer "+this.ID_SERVER+"] Schimb operatiile cheii " + creq.key + " la " + creq.newOps);
						creq.key.interestOps(creq.newOps);
					}
				}
			} catch (Exception e) {
				System.err.println("Exceptie in thread-ul Selectorului: " + e);
				e.printStackTrace();
			}
		}

		// Inchide toate ServerSocketChannel-urile.
		for (ServerSocketChannel ssc: this.serverChannels) {
			try {
				ssc.close();
			} catch (Exception e) {}
		}

		// Inchide toate SocketChannel-urile.
		for (SocketChannel schan: this.socketChannels) {
			try {
				schan.close();
			} catch (Exception e) {}
		}

		System.out.println("[NIOTCPServer "+this.ID_SERVER+"] M-am oprit");
	}

	protected void doAccept(SelectionKey key) throws Exception {
		ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
		SocketChannel socketChannel = serverSocketChannel.accept();
		socketChannel.configureBlocking(false);
		socketChannel.register(this.selector, SelectionKey.OP_READ);

		this.socketChannels.add(socketChannel);
	}
	
	protected void doRead(SelectionKey key) throws Exception {
		SocketChannel socketChannel = (SocketChannel) key.channel();
		this.rBuffer.clear();
		int numRead;

		try {
			numRead = socketChannel.read(this.rBuffer);
		} catch (Exception e) {
			numRead = -1000000000;
		}

		if (numRead <= 0) {
			System.out.println("[NIOTCPServer "+this.ID_SERVER+"] S-a inchis socket-ul asociat cheii " + key);
			key.channel().close();
			key.cancel();
			return;
		}

		byte[] rbuf = null;
		rbuf = this.readBuffers.get(key);

		int rbuflen = 0;
		if (rbuf != null) {
			rbuflen = rbuf.length;
		}

		byte[] currentBuf = this.rBuffer.array();
		System.out.println("[NIOTCPServer "+this.ID_SERVER+"] S-au citit " + numRead + " bytes de pe socket-ul asociat cheii " + key + " : " + currentBuf);

		byte[] newBuf = new byte[rbuflen + numRead];

		// Copiaza datele primite in newBuf (rbuf sunt datele primite
		// anterior si care nu formeaza o cerere completa, iar currentBuf
		// contine datele primite la read-ul curent.
		for (int i = 0; i < rbuflen; i++) {
			newBuf[i] = rbuf[i];
		}

		for (int i = rbuflen; i < newBuf.length; i++) {
			newBuf[i] = currentBuf[i - rbuflen];
		}

		int i = 0;
		int length = 0;
		// Citire dimensiune cerere
		if (i + 4 >= newBuf.length)
			return;
		length = ((128 + (int)newBuf[i]) << 24) + ((128 + (int)newBuf[i + 1]) << 16) + ((128 + (int)newBuf[i + 2]) << 8) + (128 + (int)newBuf[i + 3]);
		System.out.println("[NIOTCPServer "+this.ID_SERVER+"] S-a primit o cerere lungime = " + length + ")");
		i += 4;

		// Citeste obiectul serializat
		if (i + length <= newBuf.length) {
			Request request = (Request)Utils.deserialize(Arrays.copyOfRange(newBuf, 4, length + 4));
			
			ForwardRequest.forwardRequest(request, sobj, this.ID_SERVER, new NIOCallbackObject(this, key));
			
			i += length;
		} else
			i -= 4;

		byte[] finalBuf = null;
		if (i > 0) {
			finalBuf = new byte[newBuf.length - i];
			for (int j = i; j < newBuf.length; j++) {
				finalBuf[j - i] = newBuf[j];
			}
		} else {
			finalBuf = newBuf;
		}

		this.readBuffers.put(key, finalBuf);
	}

	protected void doWrite(SelectionKey key) throws Exception {
		SocketChannel socketChannel = (SocketChannel) key.channel();

		ArrayList<byte[]> wbuf = null;

		synchronized(key) {
			wbuf = this.writeBuffers.get(key);

			while (wbuf.size() > 0) {
				byte[] bbuf = wbuf.get(0);
				wbuf.remove(0);

				this.wBuffer.clear();
				this.wBuffer.put(bbuf);
				this.wBuffer.flip();

				int numWritten = socketChannel.write(this.wBuffer);
				System.out.println("[NIOTCPServer "+this.ID_SERVER+"] Am scris " + numWritten + " bytes pe socket-ul asociat cheii " + key);

				if (numWritten < bbuf.length) {
					byte[] newBuf = new byte[bbuf.length - numWritten];

					// Copiaza datele inca nescrise din bbuf in newBuf.
					for (int i = numWritten; i < bbuf.length; i++) {
						newBuf[i - numWritten] = bbuf[i];
					}

					wbuf.add(0, newBuf);
					break;
				}
			}

			if (wbuf.size() == 0) {
				synchronized (this.changeRequestQueue) {
					this.changeRequestQueue.add(new ChangeRequest(key, SelectionKey.OP_READ));
				}
			}
		}
	}

	// Metoda apelata de toate obiectele CallbackObject pentru a trimite rezultatele inapoi.
	public void sendData(SelectionKey key, byte[] data) {
		System.out.println("[NIOTCPServer "+this.ID_SERVER+"] Se doreste scrierea a " + data.length + " bytes pe socket-ul asociat cheii " + key);

		ArrayList<byte[]> wbuf = null;

		synchronized (key) {
			wbuf = this.writeBuffers.get(key);
			if (wbuf == null) {
				wbuf = new ArrayList<byte[]>();
				this.writeBuffers.put(key, wbuf);
			}

			wbuf.add(data);
			synchronized (this.changeRequestQueue) {
				this.changeRequestQueue.add(new ChangeRequest(key, SelectionKey.OP_READ | SelectionKey.OP_WRITE));
			}
		}

		this.selector.wakeup();
	}
}
