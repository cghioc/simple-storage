package server;

import request.DownloadRequest;
import request.ListFilesRequest;
import request.LockRequest;
import request.LoginRequest;
import request.RegisterRequest;
import request.Request;
import request.SaveRequest;
import request.UnlockRequest;
import request.UploadRequest;
import client.CallbackInterface;

public class ForwardRequest {

	public static void forwardRequest(Request request , ServiceObject sobj , int ID_SERVER, CallbackInterface callback){

		System.out.println("\n\t\t\tAM intrat in forwardRequest.");
		switch (request.getId()) {

		case Request.LOGIN_REQUEST:
			LoginRequest login = (LoginRequest)request;
			System.out.println("[ForwardRequest "+ID_SERVER+"] S-a primit o cerere " + login +" [Type: "+request.getType()+"]");
			sobj.checkUser(login.getUsername(), login.getPassword(), callback, request.getType());
			break;

		case Request.REGISTER_REQUEST:
			RegisterRequest register = (RegisterRequest)request;
			System.out.println("[ForwardRequest "+ID_SERVER+"] S-a primit o cerere " + register +" [Type: "+request.getType()+"]");
			sobj.registerUser(register.getUsername(),
					register.getPassword(), callback, request.getType());
			break;

		case Request.LIST_FILES_REQUEST:
			ListFilesRequest listFiles = (ListFilesRequest)request;
			System.out.println("[ForwardRequest "+ID_SERVER+"] S-a primit o cerere " + listFiles +" [Type: "+request.getType()+"]");
			sobj.listFiles(listFiles.getUsername(),
					listFiles.getPassword(), callback, request.getType());
			break;

		case Request.DOWNLOAD_REQUEST:
			DownloadRequest download = (DownloadRequest)request;
			System.out.println("[ForwardRequest "+ID_SERVER+"] S-a primit o cerere " + download +" [Type: "+request.getType()+"]");
			sobj.downloadFile(download.getUsername(),
					download.getFileName(), callback, request.getType());
			break;

		case Request.UPLOAD_REQUEST:
			UploadRequest upload = (UploadRequest)request;
			System.out.println("[ForwardRequest "+ID_SERVER+"] S-a primit o cerere " + upload +" [Type: "+request.getType()+"]");
			sobj.uploadFile(upload.getUsername(),
					upload.getFileName(), upload.getFile(), callback, request.getType());
			break;

		case Request.LOCK_REQUEST:
			LockRequest lock = (LockRequest)request;
			System.out.println("[ForwardRequest "+ID_SERVER+"] S-a primit o cerere " + lock +" [Type: "+request.getType()+"]");
			sobj.lockFile(lock.getUsername(),
					lock.getFileName(), callback, request.getType());
			break;

		case Request.UNLOCK_REQUEST:
			UnlockRequest unlock = (UnlockRequest)request;
			System.out.println("[ForwardRequest "+ID_SERVER+"] S-a primit o cerere " + unlock +" [Type: "+request.getType()+"]");
			sobj.unlockFile(unlock.getUsername(),
					unlock.getFileName(),  callback, request.getType());
			break;
		case Request.SAVE_REQUEST:
			SaveRequest save = (SaveRequest)request;
			System.out.println("[ForwardRequest "+ID_SERVER+"] S-a primit o cerere " + save +" [Type: "+request.getType()+"]");
			sobj.saveFile(save.getUsername(),
					save.getFileName(), save.getFile() , callback , request.getType());
			break;
		}
	}
}
