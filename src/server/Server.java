package server;

import common.StorageProps;

public class Server {
	public static void main(String args[]) {
		StorageProps.loadProperties();

		int ports = 0;
		int myID = 0;//Integer.parseInt( args[0].toString() );
		
		for(int i=0 ; i<5 ; i++){
			ServiceObject sobj = new ServiceObject(50000 + i , i);
			sobj.start();
			SimpleNIOTCPServer server = new SimpleNIOTCPServer(50000 + i, sobj , i);
			//ServerMulticastListener.loadInstance(i, StorageProps.MULTICAST_ADDR , StorageProps.MULTICAST_PORT, sobj);
			ServerMulticastListener sml = new ServerMulticastListener(i, StorageProps.MULTICAST_ADDR , StorageProps.MULTICAST_PORT, sobj);
			sml.startRunning();
			server.startRunning();
		}
		/*for (int i = 0; i < 1; i++) {
			ServiceObject sobj = new ServiceObject(50000 + i , i);
			sobj.start();
			SimpleTCPServer sts = new SimpleTCPServer(50000 + i, sobj, i);
			sts.start();
		}*/

		/*try {
			Thread.sleep(300000);
		} catch (Exception e) {
			System.err.println("Exceptie: " + e);
			e.printStackTrace();
		}

		sobj.stop();
		server.stopRunning();*/
	}
}
