package server;

import java.io.DataInputStream;
import java.net.Socket;

import request.DownloadRequest;
import request.ListFilesRequest;
import request.LockRequest;
import request.LoginRequest;
import request.RegisterRequest;
import request.Request;
import request.SaveRequest;
import request.UnlockRequest;
import request.UploadRequest;
import client.NIOCallbackObject;
import client.TCPCallbackObject;

import common.Utils;

public class TCPServerThread extends Thread {
	private Socket s;
	private int ID_SERVER;
	private ServiceObject sobj;

	public TCPServerThread(Socket s, int ID_SERVER, ServiceObject sobj) {
		this.s = s;
		this.ID_SERVER = ID_SERVER;
		this.sobj = sobj;
	}

	public void run() {
		System.out.println("[TCPServerThread] " + this.hashCode() + " running");
		try {
			DataInputStream din = new DataInputStream(this.s.getInputStream());
			

			while(true) {
				// Receive the request
				int size = din.readInt();
				System.out.println("[TCPServerThread] TCP size received " + size);
				byte[] bytes = new byte[size];
				din.readFully(bytes);
				Request request = (Request)Utils.deserialize(bytes);
				
				System.out.println("[TCPServerThread] " + this.hashCode() + " got request " + request);
				
				ForwardRequest.forwardRequest(request, sobj, this.ID_SERVER, new TCPCallbackObject(s));
				
			}
		} catch (Exception e) {
			System.err.println("TCPServerThread-" + this.hashCode() + " exception: " + e);
			e.printStackTrace();
		}
	}
}
