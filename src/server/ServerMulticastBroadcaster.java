package server;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import request.Request;

public class ServerMulticastBroadcaster {
	private String multicastAddr;
	private int multicastPort;
	private DatagramSocket socket;
	private int id;

	public ServerMulticastBroadcaster(int id, String multicastAddr, int multicastPort) {
		this.id = id;
		this.multicastAddr = multicastAddr;
		this.multicastPort = multicastPort;

		try {
			this.socket = new DatagramSocket();
		} catch (Exception e) {
			System.err.println("[ServerBroadcaster-" + this.id + "] Exceptie la creare socket UDP: " + e);
			e.printStackTrace();
		}
	}

	public void sendServerMulticast(Request servReq) {

		System.out.println("\t[ServerBroadcaster-" + this.id + "] Sending request "+servReq);
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(servReq);
			byte[] outputBuf = baos.toByteArray();

			InetAddress maddr = InetAddress.getByName(this.multicastAddr);
			DatagramPacket packet = new DatagramPacket(outputBuf, 0, outputBuf.length, maddr, this.multicastPort);
			this.socket.send(packet);
			oos.close();
			baos.close();

			System.out.println("\t[ServerBroadcaster-" + this.id + "] Sent request "+servReq);

		} catch (Exception e) {
			System.err.println("[StreamAnnouncementBroadcaster-" + this.hashCode() + "] Exceptie la send: " + e);
			e.printStackTrace();
		}
	}
}
