package server;

import java.io.DataInputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

import common.Utils;

public class ServerResponseConnector extends Thread{
	
	public static final int MaxAccepts = 10;

	private ServerSocket serverSocket;
	private int port;
	
	public ServerResponseConnector(int port) {
		this.port = port;
	}
	
	public void run() {
		try {
			this.serverSocket = new ServerSocket(port);
		} catch (Exception e) {
			System.err.println("Exceptie la crearea ServerSocket-ului: " + e);
			e.printStackTrace();
			return;
		}
	}
	
	public Object getAnswer(){
		Socket clientSocket = null;
		Object response = null;
		
		try {
			clientSocket = this.serverSocket.accept();
			ObjectInputStream din = new ObjectInputStream(clientSocket.getInputStream());
			
			System.out.println("[TCPServerResponseConnector] TCP size received ceva ceva");
			
			response = (Object) din.readObject();
			
		} catch (Exception e) {
			System.err.println("Eroare la accept: " + e);
			e.printStackTrace();
		}
		
		return response;
	}

}
