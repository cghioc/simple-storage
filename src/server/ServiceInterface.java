package server;

import client.CallbackInterface;

public interface ServiceInterface {
	public void checkUser(String username, String password, CallbackInterface ci , int req_type);
	public void registerUser(String username, String password, CallbackInterface ci , int req_type);
	public void listFiles(String username, String password, CallbackInterface ci , int req_type);
}
