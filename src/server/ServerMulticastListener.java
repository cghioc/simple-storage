package server;

import java.io.*; 
import java.net.*;

import client.TCPCallbackObject;

import request.Request;

public class ServerMulticastListener extends Thread {
	private String multicastAddr;
	private ServiceObject sobj;
	private int multicastPort;
	private boolean running;
	private MulticastSocket socket;
	private int id;
	private static ServerMulticastListener instance;
	
	public static ServerMulticastListener getInstance(){
		return instance;
	}
	
	public static void loadInstance(int id, String multicastAddr, int multicastPort, ServiceObject sobj){
		if(instance != null)
			return;
		instance = new ServerMulticastListener(id, multicastAddr, multicastPort, sobj);		
		instance.startRunning();
	}

	public ServerMulticastListener(int id, String multicastAddr, int multicastPort, ServiceObject sobj) {
		this.id = id;
		this.multicastAddr = multicastAddr;
		this.multicastPort = multicastPort;
		this.sobj = sobj;
		this.running = false;

		System.out.println("\t [ServerMulticastListener - Server "+id+"] - Starting");
		
		try {
			this.socket = new MulticastSocket(this.multicastPort);
			//this.socket.setSoTimeout(20000);
			this.socket.joinGroup(InetAddress.getByName(multicastAddr));
		} catch (Exception e) {
			System.err.println("[StreamAnnouncementBroadcaster-" + this.hashCode() + "] Exceptie la creare socket multicast: " + e);
			e.printStackTrace();
		}
	}

	public int hashCode() {
		return this.id;
	}

	public synchronized void startRunning() {
		this.running = true;
		this.start();
	}

	public synchronized boolean isRunning() {
		return this.running;
	}

	public synchronized void stopRunning() {
		this.running = false;
	}

	public void run() {
		System.out.println("[StreamAnnouncementListener-" + this.hashCode() + "] Am pornit");

		byte[] receiveBuffer = new byte[16384];

		while (this.isRunning()) {
			try {
				DatagramPacket packet = new DatagramPacket(receiveBuffer, 0, receiveBuffer.length);
				this.socket.receive(packet);

				ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(packet.getData()));
				Request request = (Request) ois.readObject();
				
				if(request!=null)
					System.err.println("\t [ServerMulticastListener - Server "+id+"] Am primit o cerere");
				
				ForwardRequest.forwardRequest(request, this.sobj, this.id, new TCPCallbackObject( new Socket( request.getIP() , request.getPort() ) ) );
				
				//ois.close();
				
			} catch (Exception e) {
				System.err.println("\t [ServerMulticastListener - Server "+id+"] Exceptie la receive: " + e);
				e.printStackTrace();
			}
		}

		try {
			this.socket.leaveGroup(InetAddress.getByName(multicastAddr));
		} catch (Exception e) {
			System.err.println("[SystemAnnouncementListener-" + this.hashCode() + "] Eroare la leaveGroup: " + e);
			e.printStackTrace();
		}

		System.out.println("[StreamAnnouncementListener-" + this.hashCode() + "] M-am oprit");
	}
}
