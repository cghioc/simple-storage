package server;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class SimpleTCPServer extends Thread {
	public static final int MaxAccepts = 4;
	public static int TOTAL_COUNT;

	private ServerSocket serverSocket;
	private int port;
	private ArrayList<TCPServerThread> al;
	private ServiceObject sobj;
	private int index;
	
	public SimpleTCPServer(int port, ServiceObject sobj, int index) {
		this.port = port;
		this.al = new ArrayList<TCPServerThread>();
		SimpleTCPServer.TOTAL_COUNT = 0;
		this.sobj = sobj;
		this.index = index;
	}
	
	public void run() {
		try {
			this.serverSocket = new ServerSocket(port);
		} catch (Exception e) {
			System.err.println("Exceptie la crearea ServerSocket-ului: " + e);
			e.printStackTrace();
			return;
		}
		
		for (int i = 0; i < MaxAccepts; i++) {
			System.out.println("[SimpleTCPServer] Accepting a new connection");

			Socket clientSocket = null;
			try {
				clientSocket = this.serverSocket.accept();
				TCPServerThread mst = new TCPServerThread(clientSocket, index, sobj);
				mst.start();
				this.al.add(mst);
			} catch (Exception e) {
				System.err.println("Eroare la accept: " + e);
				e.printStackTrace();
			}
		}
		System.out.println("[SimpleTCPServer] joining all threads");
		for (TCPServerThread tst: this.al) {
			try {
				tst.join();
			} catch (Exception e) {}
		}
		
		try {
			this.serverSocket.close();
		} catch (Exception e) {
			System.err.println("ServerSocket close exception: " + e);
			e.printStackTrace();
		}
	}
}
