package client;

import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.Socket;

import common.Utils;

public class NIOTCPClientConnector extends Thread implements ClientConnectorInterface {
	private String serverIP;
	private int serverPort;
	
	private Socket clientSocket;
	
	public NIOTCPClientConnector(String serverIP, int serverPort) {
		this.serverIP = serverIP;
		this.serverPort = serverPort;
	}
	
	public void run() {
		try {
			this.clientSocket = new Socket(this.serverIP, this.serverPort);
			
		} catch (Exception e) {
			System.err.println("TCPClient-" + this.hashCode() + " exception: " + e);
			e.printStackTrace();
		}
	}

	public void exit() {
		try {
			clientSocket.close();
		} catch (Exception e) {
			System.err.println("TCPClient-" + this.hashCode() + " exception: " + e);
			e.printStackTrace();
		}
	}

	public void sendMessage(Serializable message) {
		try {
			byte[] serialized = Utils.serialize(message);
			byte[] req = new byte[4];

			req[0] = (byte) (((serialized.length >> 24) % 256) - 128);
			req[1] = (byte) (((serialized.length >> 16) % 256) - 128);
			req[2] = (byte) (((serialized.length >> 8) % 256) - 128);
			req[3] = (byte) ((serialized.length % 256) - 128);

			System.out.println("[TCPClient-" + this.hashCode() + "] Trimite cererea "
					+ message + " lungime " + serialized.length);

			/* Send the request */
			clientSocket.getOutputStream().write(req);
			clientSocket.getOutputStream().write(serialized);
			System.out.println("A plecat");
		} catch (Exception e) {
			System.err.println("TCPClient-" + this.hashCode() + " exception: " + e);
			e.printStackTrace();
		}
	}

	public Object getAnswer() {
		Object answer = null;

		/* Wait for the answer */
		while (answer == null) {
			try {
		       // ObjectInputStream in = new ObjectInputStream(this.clientSocket.getInputStream());
		        answer = new ObjectInputStream(this.clientSocket.getInputStream()).readObject();
		        
		        if(answer == null) {
		        	System.out.println("\n\t\t answer = null");
		        	continue;
		        }
		        
		        return answer;
			} catch (Exception e) {
				System.err.println("TCPClientConnector-" + this.hashCode() + " exception: " + e);
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Try to open a socket on this port
	 */
	public boolean tryPort(String testIp, int testPort) {
		System.out.println("Trying server " + testIp + " port" + testPort);
		try {
			Socket test = new Socket(testIp, testPort);
			test.close();
			return true;
		} catch (Exception e) {
			System.out.println("Server not available");
		}
		return false;
	}
}
