package client;

import java.util.ArrayList;

import common.ServerDetails;

public class Client {
	public static void main(String args[]) {
		

		/* Sending some test requests */
		System.out.println("Trying all known servers");
		ArrayList<ServerDetails> workingServers = ClientServiceObject.checkServers();
		for (ServerDetails server : workingServers)
			System.out.println("Server " + server + " is working");

		NIOTCPClientConnector connector = new NIOTCPClientConnector("localhost", 50000);
		connector.start();
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		ClientServiceObject cso = ClientServiceObject.getInstance(connector);
		System.out.println(cso.checkUser("Mama", "tata"));
		System.out.println(cso.registerUser("Cornel", "Popcorn"));
		
		
		try {
			connector.exit();
			connector.join();
		} catch (Exception e) {
			System.out.println("Error in main client class");
			e.printStackTrace();
		}
	}
}
