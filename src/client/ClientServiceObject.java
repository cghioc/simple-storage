package client;

import java.io.File;
import java.net.Socket;
import java.util.ArrayList;

import request.DownloadRequest;
import request.ListFilesRequest;
import request.LockRequest;
import request.LoginRequest;
import request.RegisterRequest;
import request.SaveRequest;
import request.UnlockRequest;
import request.UploadRequest;

import common.ServerDetails;
import common.StorageProps;

public class ClientServiceObject implements ClientServiceInterface {
	private ClientConnectorInterface connector;
	private static ClientServiceObject instance;


	private ClientServiceObject(ClientConnectorInterface connector) {
		this.connector = connector;
	}

	public static ClientServiceObject getInstance(ClientConnectorInterface connector) {
		if (instance != null)
			return instance;
		instance = new ClientServiceObject(connector);
		return instance;
	}

	/** 
	 * Do the actual login
	 */
	public String checkUser(String username, String password) {
		/* Create a login request */
		LoginRequest login = new LoginRequest(username, password);
		this.connector.sendMessage(login);

		String answer = (String) this.connector.getAnswer();
		System.out.println("[ClientServiceObject-" + this.hashCode() + 
				"] Am primit raspunsul: " + answer);

		return answer;
	}

	public String checkUser(String username, String password, int type) {
		/* Create a login request */
		LoginRequest login = new LoginRequest(username, password);
		login.setType(type);
		this.connector.sendMessage(login);

		String answer = (String) this.connector.getAnswer();
		System.out.println("[ClientServiceObject-" + this.hashCode() + 
				"] Am primit raspunsul: " + answer);

		return answer;
	}

	/**
	 * Create a new user
	 */
	public String registerUser(String username, String password) {
		/* Create a register request */
		RegisterRequest register = new RegisterRequest(username, password);
		this.connector.sendMessage(register);

		String answer = (String)this.connector.getAnswer();
		System.out.println("[ClientServiceObject-" + this.hashCode() +
				"] Am primit raspunsul: " + answer);
		return answer;
	}

	/**
	 * List all the files for a user
	 */
	public ArrayList<String> listFiles(String username, String password) {
		/* Create a ListFilesRequest */
		ListFilesRequest listFiles = new ListFilesRequest(username, password);
		this.connector.sendMessage(listFiles);

		ArrayList<String> userFiles = (ArrayList<String>)this.connector.getAnswer();
		return userFiles;
	}

	/**
	 * Try several server ports to see which one is connected
	 */
	public static ArrayList<ServerDetails> checkServers() {
		ArrayList<ServerDetails> servers = StorageProps.getServers();
		int i = 0;

		for (i = 0; i < servers.size(); i++) {
			ServerDetails server = servers.get(i);
			try {
				Socket test = new Socket(server.getIP(), server.getPort());
				test.close();
				servers.get(i).setState(true);
			} catch (Exception e) {
				System.out.println("Server not available");
				server.setState(false);
			}
		}

		return servers;
	}

	/**
	 * Download a file from the server
	 */
	public File downloadFile(String username, String fileName) {
		DownloadRequest download = new DownloadRequest(username, fileName);
		this.connector.sendMessage(download);

		File answer = (File) this.connector.getAnswer();
		System.out.println("[ClientServiceObject-" + this.hashCode() +
				"] Am primit fisier la download ");
		return answer;
	}

	/**
	 * Upload a file to the server
	 */
	public Boolean uploadFile(String username, String fileName, File file) {
		UploadRequest message = new UploadRequest(username, fileName, file);
		this.connector.sendMessage(message);

		Boolean answer = (Boolean) this.connector.getAnswer();
		System.out.println("[ClientServiceObject-" + this.hashCode() +
				"] Am primit raspuns la upload " + answer);
		return answer;
	}

	/**
	 * Lock a file on the server
	 */
	public String lockFile(String username, String fileName) {
		LockRequest message = new LockRequest(username, fileName);
		this.connector.sendMessage(message);

		String answer = (String) this.connector.getAnswer();		
		System.out.println("[ClientServiceObject-" + this.hashCode() +
				"] Am primit raspuns la lockFile " + answer);
		return answer;
	}

	/**
	 * Unlock a file on the server
	 */
	public Boolean unlockFile(String username, String fileName) {
		UnlockRequest message = new UnlockRequest(username, fileName);
		this.connector.sendMessage(message);

		Boolean answer = (Boolean) this.connector.getAnswer();		
		System.out.println("[ClientServiceObject-" + this.hashCode() +
				"] Am primit raspuns la unlockFile " + answer);
		return answer;
	}

	public Boolean saveFile(String username, String fileName , File file) {
		SaveRequest message = new SaveRequest(username, fileName, file);
		this.connector.sendMessage(message);

		Boolean answer = (Boolean) this.connector.getAnswer();
		System.out.println("[ClientServiceObject-" + this.hashCode() +
				"] Am primit raspuns la save " + answer);
		return answer;
	}
}
