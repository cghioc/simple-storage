package client;

import java.io.Serializable;

public interface ClientConnectorInterface {
	public void sendMessage(Serializable message);
	public Object getAnswer();
}
