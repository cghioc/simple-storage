package client;

import java.io.*;
import java.nio.channels.*;

import common.NIOTCPSocketConnector;



public class NIOCallbackObject implements CallbackInterface {
	private NIOTCPSocketConnector connector;
	private SelectionKey key;
	
	public NIOCallbackObject(NIOTCPSocketConnector connector, SelectionKey key) {
		this.connector = connector;
		this.key = key;
	}
	
	public void sendResult(Object result) {
		try {
			// Serializam rezultatul.
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(result);
			byte[] outputBuf = baos.toByteArray();
			
			// Apoi il dam spre a fi trimis catre client.
			this.connector.sendData(this.key, outputBuf);
		} catch (Exception e) {
			System.err.println("sendResult exception: " + e);
			e.printStackTrace();
		}
	}
}
