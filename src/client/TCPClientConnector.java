package client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;

import common.Utils;

public class TCPClientConnector extends Thread implements ClientConnectorInterface {
	private String serverIP;
	private int serverPort;
	
	private Socket clientSocket;
	
	public TCPClientConnector(String serverIP, int serverPort) {
		this.serverIP = serverIP;
		this.serverPort = serverPort;
	}

	public void run() {
		try {
			this.clientSocket = new Socket(this.serverIP, this.serverPort);
			
		} catch (Exception e) {
			System.err.println("TCPClient-" + this.hashCode() + " exception: " + e);
			e.printStackTrace();
		}
	}
	
	public void exit() {
		try {
			clientSocket.close();
		} catch (Exception e) {
			System.err.println("TCPClient-" + this.hashCode() + " exception: " + e);
			e.printStackTrace();
		}
	}

	@Override
	public void sendMessage(Serializable message) {
		try {
			byte[] serialized = Utils.serialize(message);
			int size = serialized.length;
			DataOutputStream dos = new DataOutputStream(clientSocket.getOutputStream());
			dos.writeInt(size);
			dos.write(serialized);
			System.out.println("A plecat");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Object getAnswer() {
		Object answer = null;

		/* Wait for the answer */
		while (answer == null) {
			try {
		       // ObjectInputStream in = new ObjectInputStream(this.clientSocket.getInputStream());
		        answer = new ObjectInputStream(this.clientSocket.getInputStream()).readObject();
		        
		        if(answer == null) {
		        	System.out.println("\n\t\t answer = null");
		        	continue;
		        }
		        
		        return answer;
			} catch (Exception e) {
				System.err.println("TCPClientConnector-" + this.hashCode() + " exception: " + e);
				e.printStackTrace();
			}
		}
		return null;
	}

}
