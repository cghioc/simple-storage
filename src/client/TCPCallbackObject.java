package client;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class TCPCallbackObject implements CallbackInterface{
	private Socket socket;
	public TCPCallbackObject(Socket socket) {
		this.socket = socket;
	}
	
	@Override
	public void sendResult(Object obj) {
		try {
			ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
			out.writeObject(obj);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
