package client;

import java.io.File;
import java.util.ArrayList;

public interface ClientServiceInterface {
	public String checkUser(String username, String password);
	public String registerUser(String username, String password);
	public ArrayList<String> listFiles(String username, String password);
	public File downloadFile(String username, String fileName);
	public Boolean uploadFile(String username, String fileName, File file);
	public String lockFile(String username, String fileName);
}
