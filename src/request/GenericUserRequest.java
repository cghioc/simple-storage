package request;

public class GenericUserRequest extends Request{
	private static final long serialVersionUID = -7733597302823626548L;
	protected String username;
	public GenericUserRequest(String username) {
		super();
		this.username = username;
	}

	public String getUsername() {
		return this.username;
	}
}
