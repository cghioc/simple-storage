package request;

public class UnlockRequest extends DownloadRequest {
	private static final long serialVersionUID = -2108781930946643014L;

	public UnlockRequest(String username, String fileName) {
		super(username, fileName);
		this.id = Request.UNLOCK_REQUEST;
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Lock file" + fileName + " user " + username;
	}
}
