package request;

import java.io.Serializable;

public class Request implements Serializable {
	private static final long serialVersionUID = 1034336712296084628L;
	public static final int LOGIN_REQUEST		= 0x01;
	public static final int REGISTER_REQUEST	= 0x02;
	public static final int LIST_FILES_REQUEST	= 0x03;
	public static final int DOWNLOAD_REQUEST	= 0x04;
	public static final int UPLOAD_REQUEST		= 0x05;
	public static final int LOCK_REQUEST		= 0x06;
	public static final int UNLOCK_REQUEST		= 0x07;
	public static final int SAVE_REQUEST		= 0x08;
	
	public static final int CLIENT_SERVER		= 0x20;
	public static final int SERVER_SERVER		= 0x21;
	
	private int port;
	private String IP;
	
	protected int id;
	protected int type = Request.CLIENT_SERVER;

	public int getId() {
		return this.id;
	}
	public int getType() {
		return this.type;
	}
	
	public int getPort(){
		return this.port;
	}
	
	public String getIP(){
		return this.IP;
	}
	
	public void setPort(int port){
		this.port = port;
	}
	
	public void setIP(String IP){
		this.IP = IP;
	}
}
