package request;

public class DownloadRequest extends GenericUserRequest {

	private static final long serialVersionUID = -8687711706149796862L;
	protected String fileName;

	public DownloadRequest(String username, String fileName) {
		super(username);
		this.fileName = fileName;
		this.id = Request.DOWNLOAD_REQUEST;
	}

	@Override
	public int getId() {
		return this.id;
	}

	public String getFileName() {
		return this.fileName;
	}
}
