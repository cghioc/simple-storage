package request;


public class RegisterRequest extends LoginRequest{

	private static final long serialVersionUID = 718688537072500590L;
	public static final String REGISTER_OK		= "REGISTERED";
	public static final String REGISTER_FAIL	= "REGISTERED_FAIL";

	public RegisterRequest(String username, String password){
		super(username, password);
		this.id = Request.REGISTER_REQUEST;
	};

	@Override
	public int getId() {
		return this.id;
	}
}
