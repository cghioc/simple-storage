package request;

public class ListFilesRequest extends LoginRequest {
	private static final long serialVersionUID = -299262002345947195L;

	public ListFilesRequest(String username, String password) {
		super(username, password);
		this.id = Request.LIST_FILES_REQUEST;
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public String toString() {
		return "List files user " + username;
	}
}
