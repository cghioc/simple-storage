package request;

public class LockRequest extends DownloadRequest {
	private static final long serialVersionUID = -2108781930946643014L;
	public static final String LOCK_OK = "OK";
	public static final String LOCK_FAIL = "FAIL";

	public LockRequest(String username, String fileName) {
		super(username, fileName);
		this.id = Request.LOCK_REQUEST;
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Lock file" + fileName + " user " + username;
	}
}
