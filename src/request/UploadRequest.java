package request;

import java.io.File;

public class UploadRequest extends DownloadRequest {
	private static final long serialVersionUID = -8431705351886307528L;
	private File file;

	public UploadRequest(String username, String fileName, 
			File file) {
		super(username, fileName);
		this.file = file;
		this.id = Request.UPLOAD_REQUEST;
	}

	@Override
	public int getId() {
		return this.id;
	}

	public File getFile() {
		return this.file;
	}
}
