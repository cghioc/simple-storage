package request;


public class LoginRequest extends Request{

	private static final long serialVersionUID 	= 5447906608408524685L;
	public static final String LOGIN_OK			= "OK";
	public static final String LOGIN_FAIL		= "FAIL";
	protected String username, password;

	public LoginRequest(String username, String password) {
		this.username = username;
		this.password = password;
		this.id = Request.LOGIN_REQUEST;
	}

	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}

	@Override
	public int getId() {
		return Request.LOGIN_REQUEST;
	}
	
	@Override
	public String toString() {
		return "Login U: " + username + ", p: " + password;
	}
	
	public void setType(int _type){
		this.type = _type;
	}
}

