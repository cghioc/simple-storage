package request;

import java.io.File;

public class SaveRequest extends DownloadRequest {
	private static final long serialVersionUID = -8431705351886307528L;
	private File file;

	public SaveRequest(String username, String fileName, 
			File file) {
		super(username, fileName);
		this.file = file;
		this.id = Request.SAVE_REQUEST;
	}

	@Override
	public int getId() {
		return this.id;
	}

	public File getFile() {
		return this.file;
	}
}
