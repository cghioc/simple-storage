package common;

public class ServerDetails {
	private String IP;
	private int port;
	private boolean state;

	public ServerDetails(String IP, int port) {
		this.IP = IP;
		this.port = port;
	}

	public String getIP() {
		return this.IP;
	}

	public int getPort() {
		return this.port;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public boolean getState() {
		return this.state;
	}

	public String toString() {
		return "IP: " + this.IP + ", port: " + this.port + " up? " + this.state;
	}
}
