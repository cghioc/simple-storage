package common;

import java.util.ArrayList;
import java.io.*;
import org.ini4j.*;

public class StorageProps {
	public static final String CONFIG_FILE			= "conf/props.ini";
	public static final String SOCKET_TCP			= "tcp";
	public static final String SOCKET_UDP			= "udp";

	public static final String TCP_SOCKET_BLOCKING 	= "blocking";
	public static final String TCP_SOCKET_NIO		= "nio";

	public static String SOCKET_TYPE;
	public static String TCP_SOCKET_TYPE;
	public static int SERVER_NO;
	public static int THREAD_NO;
	public static String MULTICAST_ADDR;
	public static int MULTICAST_PORT;

	private static ArrayList<ServerDetails> servers;

	public static void loadProperties() {
		System.out.println("Loading properties");
		Ini ini;
		servers = new ArrayList<ServerDetails>();
		
		
		/* Open configuration file */
		try {
			ini = new Ini(new File(CONFIG_FILE));
		} catch (Exception e) {
			System.err.println("Exception opening the config file:" + e);
			e.printStackTrace();
			return;
		}
		Ini.Section communication = ini.get("comm");
		Ini.Section serverInfo = ini.get("server");

		
		/* Extract communication settings */
		SOCKET_TYPE = communication.get("socket_type");
		TCP_SOCKET_TYPE = communication.get("tcp_socket_type");

		
		/* Extract server settings */
		THREAD_NO = Integer.parseInt(serverInfo.get("thread_no"));
		SERVER_NO = Integer.parseInt(serverInfo.get("server_no"));
		for (int i = 0; i < SERVER_NO; i++)
			servers.add(new ServerDetails(serverInfo.get("server" + i),
					Integer.parseInt(serverInfo.get("port" + i))));
		
		MULTICAST_ADDR = "225.4.5.6";
		MULTICAST_PORT = 20002;
		
		System.out.println("Properties loaded");
	}

	/* Return the list of all servers */
	public static ArrayList<ServerDetails> getServers() {
		if (servers != null)
			return servers;
		loadProperties();
		return servers;
	}
}
