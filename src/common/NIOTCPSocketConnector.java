package common;

import java.nio.channels.*;

public interface NIOTCPSocketConnector {
	public void sendData(SelectionKey key, byte[] data);
}
